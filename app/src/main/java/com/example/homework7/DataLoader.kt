package com.example.homework7

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("http://www.mocky.io/v2/")
        .build()

    private fun callback(customCallback: CustomCallback) = object: Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("getRequest", "${t.message}")
            customCallback.onFailure(t.message.toString())
        }
        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("getRequest", "${response.body()}")
            customCallback.onSuccess(response.body().toString())
        }
    }

    private var service = retrofit.create(ApiRetrofit::class.java)
    fun getRequest(path: String, customCallback: CustomCallback) {
        val call = service.getRequest(path)
        call.enqueue(callback(customCallback))
    }

    fun postRequest(path:String, parameters: MutableMap<String, String>,customCallback: CustomCallback) {
        val call = service.postRequest(path, parameters)
        call.enqueue(callback(customCallback))
    }
}

interface ApiRetrofit {
    @GET("{path}")
    fun getRequest(@Path("path") path: String?): Call<String>
    @FormUrlEncoded
    @POST("{path}")
    fun postRequest(@Path("path") path: String?, parameters:Map<String, String>): Call<String>
}